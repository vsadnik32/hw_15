<?php
class GoodsItem extends Item
{
	protected $discount;

	public function __construct($title,$price,$discount)
	{
		$this->title=$title;
	    $this->type = static::getType();
	    $this->price=$price;
	    $this->discount=$discount;
	} 

	public static function getType()
	{
		return 'goods';
	}

	public function getPrice()//обсолютная скидка
	{
		return $this->price = $this->price-$this->discount;
	}

		public function getSummaryLine()
	{
		$str = $this->title .' '. $this->type .' '. $this->price. ' ';
		return $str;
	}
}