<?php
ini_set('error_reporting', E_ALL);
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);

abstract class Item implements Writable
{
	private $title;
	private $type;
	private $price;

	public function __construct($title,$price) //конструктор
	{
		$this->title = $title;
		$this->price = $price;
		$this->type = static::getType();
	}

	public abstract function getPrice();

	public function getTitle()// гетер для title
	{
		return $this->title;
	}

	public static function getType() // статический гетер для type
	{
		return 'base_item';
	}

	public function getSummaryLine()
	{
		$str = $this->title .' '. $this->type .' '. $this->price. ' ';
		return $str;
	}

	
}
