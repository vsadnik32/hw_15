<?php
ini_set('error_reporting', E_ALL);
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);

class Food extends Item
{

	public function __construct($title,$price)
	{
		$this->title=$title;
	    $this->type = static::getType();
	    $this->price=$price;
	} 

	
	public static function getType()
	{
		return 'food';
	}
	public function getprice()
	{
		$disc = 10;
		$this->price=$this->price / 100 * $disc;
		return $this->price;
	}
	public function getSummaryLine()
	{
		$str = $this->title .' '. $this->type .' '. $this->price. ' ';
		return $str;
	}


}